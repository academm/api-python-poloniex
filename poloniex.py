import requests
import time
import hmac,hashlib
from urllib.parse import urlencode as _urlencode


def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    return time.mktime(time.strptime(datestr, format))


class poloniex:
    def __init__(self, APIKey, Secret):
        self.APIKey = APIKey
        self.Secret = Secret

    def post_process(self, before):
        after = before
        if ('return' in after):
            if (isinstance(after['return'], list)):
                for x in range(0, len(after['return'])):
                    if (isinstance(after['return'][x], dict)):
                        if ('datetime' in after['return'][x] and 'timestamp' not in after['return'][x]):
                            after['return'][x]['timestamp'] = float(createTimeStamp(after['return'][x]['datetime']))
        return after

    def api_query(self, command, req={}):
        if command == "returnTicker" or command == "return24hVolume":
            ret = requests.get('https://poloniex.com/public?command=' + command)
            return ret.json()
        elif command == "returnOrderBook":
            ret = requests.get('https://poloniex.com/public?command=' + command + '&currencyPair=' + str(req['currencyPair']))
            return ret.json()
        elif command == "returnMarketTradeHistory":
            ret = requests.get('https://poloniex.com/public?command=' + "returnTradeHistory" + '&currencyPair=' + str(req['currencyPair']))
            return ret.json()
        else:
            req['command'] = command
            req['nonce'] = int(time.time() * 1000)
            post_data =_urlencode(req)
            sign = hmac.new(self.Secret, post_data.encode('utf-8'), hashlib.sha512).hexdigest()
            headers = {
                'Sign': sign,
                'Key': self.APIKey
            }

            ret = requests.post('https://poloniex.com/tradingApi', data=req, headers=headers)
            return ret.json()

    def returnTicker(self):
        return self.api_query("returnTicker")

    def return24hVolume(self):
        return self.api_query("return24hVolume")

    def returnOrderBook (self, currencyPair):
        return self.api_query("returnOrderBook", {'currencyPair': currencyPair})

    def returnMarketTradeHistory (self, currencyPair):
        return self.api_query("returnMarketTradeHistory", {'currencyPair': currencyPair})

    def returnBalances(self):
        return self.api_query('returnBalances')

    def returnOpenOrders(self,currencyPair):
        return self.api_query('returnOpenOrders',{"currencyPair":currencyPair})

    def returnTradeHistory(self, currencyPair):
        return self.api_query('returnTradeHistory',{"currencyPair":currencyPair})

    def buy(self,currencyPair,rate,amount):
        return self.api_query('buy',{"currencyPair":currencyPair,"rate":rate,"amount":amount})

    def sell(self,currencyPair,rate,amount):
        return self.api_query('sell',{"currencyPair":currencyPair,"rate":rate,"amount":amount})

    def cancel(self,currencyPair,orderNumber):
        return self.api_query('cancelOrder',{"currencyPair":currencyPair,"orderNumber":orderNumber})

    def withdraw(self, currency, amount, address):
        return self.api_query('withdraw',{"currency":currency, "amount":amount, "address":address})