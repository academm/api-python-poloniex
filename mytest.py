import poloniex
import pprint
import mykey

polo = poloniex.poloniex(mykey.APIKey, mykey.Secret)


def mybal():
    balance = polo.returnBalances()
    print("I have %s ETH!" % balance['ETH'])
    print("I have %s BTC!" % balance['BTC'])


def orders():
    orderbook = polo.returnOrderBook('BTC_ETH&depth=5')
    asks = orderbook['asks']
    bids = orderbook['bids']
    print('%-30s%-30s' % ("Sell Orders", "Buy Orders"))
    for a in range(5):
        print('%-30s%-30s' % (asks[a], bids[a]))


def history():
    tradehistory = polo.returnTradeHistory('BTC_ETH')
    print(tradehistory)


def open(currencyPair = 'BTC_ETH'):
    oo = polo.returnOpenOrders(str(currencyPair).upper())
    print(oo)


def buy(currencyPair = 'BTC_ETH', rate=0, amount=0):
    b = polo.buy(str(currencyPair).upper(), rate, amount)
    print(b)


def sell(currencyPair = 'BTC_ETH', rate=0, amount=0):
    s = polo.sell(str(currencyPair).upper(), rate, amount)
    print(s)


def cancel(currencyPair, orderNumber):
    can = polo.cancel(str(currencyPair).upper(), orderNumber)
    print(can)
